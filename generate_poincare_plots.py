import os
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

import sys
modDirc = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(modDirc)

from plot_define import plot_define

import databaseTools.functions as fun

import flfTools.flf_class as fc
import flfTools.flf_operations as fo

import vmecTools.wout_files.wout_read as wr
import vmecTools.wout_files.booz_read as br


config_tag = 'above_8div7'
wout_dirc = os.path.join(os.getcwd(), 'wout_%s' % config_tag)

# Read in configuration currents #
conID_path = os.path.join(os.getcwd(), 'conIDs_%s.txt' % config_tag)
with open(conID_path, 'r') as f:
    lines = f.readlines()
    conID_crnts = {}
    for line in lines:
        conID = line.strip()
        main, aux = fun.readCrntConfig(conID)
        conID_crnts[conID] = -10722.*np.r_[main, 14*aux]

# Read in Wout Iota for QHS #
qhs_dirc = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0')
wout = wr.readWout(qhs_dirc)
iota_qhs = wout.iota(wout.s_grid)

# Read Boozer Data QHS #
booz_qhs = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'boozmn_wout_HSX_main_opt0_64_16.nc')
booz = br.readBooz(booz_qhs)
sym_qhs = booz.qhs_metric(booz.s_dom)

# Read Boozer Data Flip14 #
booz_qhs = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_3', 'job_150', 'boozmn_wout_HSX_main_opt0_64_16.nc')
booz = br.readBooz(booz_qhs)
sym_flip = booz.qhs_metric(booz.s_dom)

# flf Parameters #
dstp = 1
rots = 500

dphi = dstp * (np.pi/180)
stps = int(2 * np.pi / dphi)
npts = int(rots*stps)

mgrid_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'mgrid_hsx_wmain.nc')

mod_dict = {'points_dphi': dphi,
            'n_iter': npts,
            'mgrid_file': mgrid_path}

# Make Plot #
for idx, key in enumerate(conID_crnts):
    print(key+' ({}|{})'.format(idx+1, len(conID_crnts)))
    conID = key
    crnt = conID_crnts[key]

    # Define Plotting Axis #
    plot = plot_define(lineWidth=2)
    plt = plot.plt

    fig = plt.figure(tight_layout=True, figsize=(14, 12))
    gs = mpl.gridspec.GridSpec(4, 4)

    ax1 = fig.add_subplot(gs[0:2, 0:3])
    ax2 = fig.add_subplot(gs[0:2, 3])
    ax3 = fig.add_subplot(gs[2:4, 0:2])
    ax4 = fig.add_subplot(gs[2:4, 2:4])

    ax1.set_aspect('equal')
    ax2.set_aspect('equal')

    # Make Poincare Plot #
    poin_path = os.path.join(wout_dirc, 'poincare_data_%s.h5' % conID)
    if not os.path.isfile(poin_path):
        mod_dict['mgrid_currents'] = ' '.join(['{}'.format(c) for c in crnt])
        flf = fc.flf_wrapper('HSX')
        flf.change_params(mod_dict)
        init_point = np.array([1.6, 0, .0*np.pi])
        ma_pnt, lcfs_pnt = flf.find_boundaries(init_point, 4)
        flf.save_read_out_domain('core', ma_pnt, lcfs_pnt, 25, fileName=poin_path)

    # fo.plot_vessel(ax1, init_point[2])
    fo.plot_poincare(poin_path, .25*np.pi, fig, ax1)
    fo.plot_poincare(poin_path, 0, fig, ax2)

    # Read Wout Data #
    name = 'wout_HSX_%s.nc' % conID
    wout = wr.readWout(wout_dirc, name=name)

    u_dom = np.linspace(-np.pi, np.pi, 251)
    wout.transForm_2D_sSec(1, u_dom, np.array([0, 0.25*np.pi]), ampKeys=['R', 'Z'])
    R_grid = wout.invFourAmps['R']
    Z_grid = wout.invFourAmps['Z']
    ax1.plot(R_grid[1], Z_grid[1], c='tab:red', linewidth=4, label='VMEC (s=1)')
    ax2.plot(R_grid[0], Z_grid[0], c='tab:red', linewidth=4)

    # Plot Iota #
    iota = wout.iota(wout.s_grid)
    ax3.plot(wout.s_grid, iota, c='tab:blue')
    # ax3.plot(wout.s_grid, iota_qhs, c='k', ls='--')

    if config_tag == 'above_4div4_below_8div7':
        ax3.plot([0, 1], [1.]*2, c='k', ls='--')
        ax3.plot([0, 1], [16./15]*2, c='k', ls='-.')
        ax3.plot([0, 1], [12./11]*2, c='k', ls=':')
        ax3.plot([0, 1], [8./7]*2, c='k', ls='--')

        ax3.text(0.25, (4./4)+gap, '4/4', fontsize=16)
        ax3.text(0.25, (16./15)+gap, '16/15', fontsize=16)
        ax3.text(0.25, (12./11)+gap, '12/11', fontsize=16)
        ax3.text(0.25, (8./7)+gap, '8/7', fontsize=16)

    elif config_tag == 'above_8div7':
        ax3.plot([0, 1], [8./7]*2, c='k', ls='--')
        ax3.plot([0, 1], [12./10]*2, c='k', ls='-.')
        ax3.plot([0, 1], [16./13]*2, c='k', ls=':')
        ax3.plot([0, 1], [4./3]*2, c='k', ls='--')

        gap = 0.002
        ax3.text(0.75, (8./7)+gap, '8/7', fontsize=16)
        ax3.text(0.25, (12./10)+gap, '12/10', fontsize=16)
        ax3.text(0.25, (16./13)+gap, '16/13', fontsize=16)
        ax3.text(0.25, (4./3)+gap, '4/3', fontsize=16)

    # Read Boozer Data #
    booz_name = 'boozmn_wout_HSX_%s.nc' % conID
    booz = br.readBooz(os.path.join(wout_dirc, booz_name))
    sym_metric = booz.qhs_metric(booz.s_dom)
    ax4.plot(booz.s_dom, sym_qhs, c='k', ls='--', label='QHS')
    ax4.plot(booz.s_dom, sym_flip, c='tab:red', ls='-.', label='Mirror')
    ax4.plot(booz.s_dom, sym_metric, c='tab:blue')

    # Axis Labels #
    ax3.set_xlabel(r'$s$')
    ax3.set_ylabel(r'$\iota/2\pi$')

    ax4.set_xlabel(r'$s$')
    ax4.set_ylabel(r'$\mathcal{Q}$')

    # Axis Legends #
    ax1.legend()
    ax4.legend()

    # Axis Grids #
    ax3.grid()
    ax4.grid()

    # Axis Limits #
    if config_tag == 'above_4div4_below_8div7':
        ax3.set_ylim(0.999, 1.153)
    elif config_tag == 'above_8div7':
        ax3.set_ylim(1.14, 1.35)

    ax3.set_xlim(0, 1)
    ax4.set_xlim(0, 1)
    ax4.set_ylim(0, ax4.get_ylim()[1])

    save_path = os.path.join('poincare_plots_%s' % config_tag, conID+'.png')
    plt.savefig(save_path)
