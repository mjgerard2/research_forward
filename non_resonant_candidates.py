import os, sys
import numpy as np
import vmecTools.wout_files.wout_read as wr


# Get Configuration ID list #
con_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'config_list.txt')
with open(con_path, 'r') as f:
    lines = f.readlines()
    conID_list = []
    for line in lines:
        conID_list.append(line.strip())

# Read iota #
conID_above = []
conID_below = []
base_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs')
for conID in conID_list:
    ID = conID.split('-')
    mainID = 'main_coil_{}'.format(ID[0])
    setID = 'set_{}'.format(ID[1])
    jobID = 'job_{}'.format(ID[2])

    wout_path = os.path.join(base_path, mainID, setID, jobID)
    wout = wr.readWout(wout_path)
    iota = wout.iota(wout.s_grid)

    cnt_above = iota[iota > 8./7].shape[0]
    cnt_below = iota[(iota > 1.) & (iota < 8./7)].shape[0]

    if cnt_above == wout.ns:
        conID_above.append(conID)
    elif cnt_below == wout.ns:
        conID_below.append(conID)

# Print conIDs #
above_path = os.path.join('conIDs_above_8div7.txt')
with open(above_path, 'w') as f:
    for conID in conID_above:
        f.write(conID+'\n')

below_path = os.path.join('conIDs_above_4div4_below_8div7.txt')
with open(below_path, 'w') as f:
    for conID in conID_below:
        f.write(conID+'\n')
