import os
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

import sys
modDirc = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(modDirc)

from plot_define import plot_define

import databaseTools.functions as fun

import flfTools.flf_class as fc
import flfTools.flf_operations as fo

import vmecTools.wout_files.wout_read as wr
import vmecTools.wout_files.booz_read as br


config_tag = 'above_8div7'
wout_dirc = os.path.join(os.getcwd(), 'wout_%s' % config_tag)

# Read in configuration currents #
conID_path = os.path.join(os.getcwd(), 'conIDs_%s.txt' % config_tag)
with open(conID_path, 'r') as f:
    lines = f.readlines()
    conID_crnts = {}
    for line in lines:
        conID = line.strip()
        main, aux = fun.readCrntConfig(conID)
        conID_crnts[conID] = -10722.*np.r_[main, 14*aux]

# flf Parameters #
dstp = 1
rots = 500

dphi = dstp * (np.pi/180)
stps = int(2 * np.pi / dphi)
npts = int(rots*stps)

mgrid_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'mgrid_hsx_wmain.nc')

mod_dict = {'points_dphi': dphi,
            'n_iter': npts,
            'mgrid_file': mgrid_path}

# Make Plot #
for idx, key in enumerate(conID_crnts):
    print(key+' ({}|{})'.format(idx+1, len(conID_crnts)))
    conID = key
    crnt = conID_crnts[key]

    # Make Poincare Plot #
    poin_path = os.path.join(wout_dirc, 'poincare_data_%s.h5' % conID)
    if not os.path.isfile(poin_path):
        mod_dict['mgrid_currents'] = ' '.join(['{}'.format(c) for c in crnt])
        flf = fc.flf_wrapper('HSX')
        flf.change_params(mod_dict)
        init_point = np.array([1.51, 0, .0*np.pi])
        ma_pnt, lcfs_pnt = flf.find_boundaries(init_point, 4)

        if False:
            # Define Plotting Axis #
            plot = plot_define(lineWidth=2)
            plt = plot.plt

            fig, ax = plt.subplots(1, 1, tight_layout=True)
            flf.read_out_point(lcfs_pnt, ax=ax, DeBug=True)
            plt.show()

        flf.save_read_out_domain('core', ma_pnt, lcfs_pnt, 25, fileName=poin_path)
