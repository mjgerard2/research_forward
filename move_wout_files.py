import os


# Read in configuration currents #
conID_path = os.path.join('conIDs_above_4div4_below_8div7.txt')
with open(conID_path, 'r') as f:
    lines = f.readlines()
    for line in lines:
        conID = line.strip().split('-')

        mainID = 'main_coil_{}'.format(conID[0])
        setID = 'set_{}'.format(conID[1])
        jobID = 'job_{}'.format(conID[2])

        wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', mainID, setID, jobID, 'wout_HSX_main_opt0.nc')
        wout_new_path = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'research_forward', 'wout_above_4div4_below_8div7', 'wout_HSX_{}-{}-{}.nc'.format(conID[0], conID[1], conID[2]))
        os.system('cp '+wout_path+' '+wout_new_path)
