import os, sys
import numpy as np

WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

import databaseTools.functions as fun


# Read in configuration currents #
# conID_path = os.path.join('conIDs_above_4div4_below_8div7.txt')
conID_path = os.path.join('conIDs_above_8div7.txt')
with open(conID_path, 'r') as f:
    lines = f.readlines()
    conID_crnts = {}
    for line in lines:
        conID = line.strip()
        main, aux = fun.readCrntConfig(conID)
        conID_crnts[conID] = -10722.*np.r_[main, 14*aux]

# crnt_file = os.path.join('conIDs_wCrnts_above_4div4_above_8div7.txt')
crnt_file = os.path.join('conIDs_wCrnts_above_8div7.txt')
with open(crnt_file, 'w') as f:
    f.write('Configuration ID main_coil_1 main_coil_2 main_coil_3 main_coil_4 main_coil_5 main_coil_6 aux_coil_1 aux_coil_2 aux_coil_3 aux_coil_4 aux_coil_5 aux_coil_6\n')
    for conID in conID_crnts:
        crnts = conID_crnts[conID]
        crnts_str = ' '.join(['{}'.format(crnt) for crnt in crnts])
        f.write(conID+' '+crnts_str+'\n')
